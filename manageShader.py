import maya.cmds as mc
import maya.mel as mel
import json
import re
import os
from pprint import pprint

class ManageShader(object):
	def __init__(self):
		super(ManageShader,self).__init__()
		self.path = self.fileP = mc.file(q=True,exn=True)
		self.nameFile = self.fileName = re.findall(r'\w+',self.fileP)[-2]
		self.path = self.path.split(self.nameFile)[0]
		self.data = {}

	def renameFile(self):
		step = re.findall(r'\w+_\w+_(\w+)_\w+_v[0-9]{3}',self.fileName)[0]
		self.nameFile = self.nameFile.replace(step,'material')
		task = re.findall(r'\w+_\w+_\w+_(\w+)_v[0-9]{3}',self.fileName)[0]
		self.nameFile = self.nameFile.replace(task,'shader')
		return self.nameFile

	def rePath(self):
		self.path = self.path.replace('work','publish')
		step = re.findall(r'\w+_\w+_(\w+)_\w+_v[0-9]{3}',self.fileName)[0]
		self.path = self.path.replace(step.capitalize(),'Material')
		self.path = self.path.split('maya/scenes/')[0]
		return self.path

		
	def writeJSON(self):
		objs = mc.ls(type = "mesh")
		value = []
		for obj in objs:		
			sg = mc.listConnections(obj, s=False, d=True, type='shadingEngine')[0]
			if not sg in self.data.keys():
				self.data[sg] = [obj]
			else:
				self.data[sg].append(obj)
		pprint(self.data) 
		jsonPath = '%sdata_v003.json'%self.rePath()
		#print jsonPath
		with open(jsonPath, 'w') as outfile:  
			json.dump(self.data, outfile, indent=4)

	def exportMaterial(self):
		exportPath = '%s%s.ma'%(self.rePath(),self.renameFile())
		melCommand = 'file -force -options "v=0;" -typ "mayaAscii" -pr -es "%s";'%exportPath
		sels = self.data.keys()
		mc.select(sels,noExpand=True)
		print exportPath
#		print melCommand
		mel.eval(melCommand)

	def importMaterial(self,path): #path + name of material.ma
		#print path
		mc.file(path,i=True, defaultNamespace = False)
		name = path.split('/')[-1]
		filepath = path.split('/%s'%name)[0]
		jason = ''
		shader_dict = {}
		for i in os.listdir(filepath):
			if 'json' in i :
				jason = i
		jsonPath = '%s/%s'%(filepath,jason)
		with open(jsonPath,'r') as json_data:
			shader_dict = json.load(json_data)
		#pprint(shader_dict)
		return shader_dict

	def replaceMaterial(self,json_data):
		#pprint(json_data)
		for key,value in json_data.iteritems():
			for i in value:
				sel = mc.listRelatives(i,p=True)[0]
				mc.select(sel)
				#print mc.listRelatives(key,s =True)
				#mc.hyperShade(key,a=True)
				mc.sets(sel, e=True, forceElement=key)





		




