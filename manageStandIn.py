import maya.cmds as mc
import maya.mel as mel
import re

class ManageStandIn(object):
	def __init__(self):
		super(ManageStandIn, self).__init__()
		self.path = self.fileP = mc.file(q=True,exn=True)
		self.nameFile = re.findall(r'\w+',self.fileP)[-2]
		self.path = self.path.split(self.nameFile)[0]
		self.data = {}

	def renameStandIn(self):
		step = re.findall(r'\w+_\w+_(\w+)_\w+_v[0-9]{3}',self.nameFile)[0]
		self.nameFile = self.nameFile.replace(step,'model')
		task = re.findall(r'\w+_\w+_\w+_(\w+)_v[0-9]{3}',self.nameFile)[0]
		self.nameFile = self.nameFile.replace(task,'standIn')
		return self.nameFile

	def rePath(self):
		self.path = self.path.replace('work','publish')
		step = re.findall(r'\w+_\w+_(\w+)_\w+_v[0-9]{3}',self.nameFile)[0]
		self.path = self.path.replace(step.capitalize(),'Cache')
		self.path = self.path.split('maya/scenes/')[0]
		return self.path

	def export_standin(self):
#		print 'export'
		pathExport = '%s%s.ass'%(self.rePath(),self.renameStandIn())
		melCommand = 'arnoldExportAss -f "%s" -s -shadowLinks 1 -mask 6399 -boundingBox;'%pathExport
		mel.eval(melCommand)
#		print melCommand

	def import_standin(self,path,name):
		'''if '/' in path:
			name = path.split('/')[-1]
		else:
			name = path.split('\\')[-1]
		name = name.split('.')[0]'''
		melCommand = 'createNode "aiStandIn";'
		node_standin = mel.eval(melCommand)
		transform = mc.listRelatives(node_standin, p=True)[0]
		mc.rename(transform,'%s'%name)
		mc.setAttr('%s.dso'%node_standin, path, type='string')

		
		
