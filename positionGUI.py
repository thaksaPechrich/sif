import os
import maya.cmds as mc
import maya.OpenMayaUI as omui
import re

import managePosition as mp
reload(mp)

from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *
from shiboken2 import wrapInstance
from pprint import pprint 

PTR = wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)
PATH = os.path.split(__file__)[0].replace("\\","/")

class PositionGUI(QDialog):
	def __init__(self,parent=''):
		super(PositionGUI, self).__init__()
		self.json = {}
		self.filePath = ''
		self.resize(300,250)
		self.setWindowTitle('Manage Position')
		self.main_layout = QVBoxLayout()
		self.setLayout(self.main_layout)
		#self.setFixedSize(300,250)
		#self.main_layout.setAlignment(Qt.AlignTop)
		#self.main_layout.setAlignment(Qt.AlignHCenter)
		#self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
######## Export Position ##########
		self.export_grpbox = QGroupBox('Export Position: ')
		self.export_layout = QVBoxLayout()
		# self.export_layout.setAlignment(Qt.AlignTop)

		self.export_button = QPushButton('export')
		self.export_button.clicked.connect(self.export_with_json)
		self.export_layout.addWidget(self.export_button)

		self.export_grpbox.setLayout(self.export_layout)
		self.main_layout.addWidget(self.export_grpbox)
######## import Position ##########
		self.import_grpbox = QGroupBox('Import Position: ')
		self.import_layout = QVBoxLayout()
		self.import_layout.setAlignment(Qt.AlignTop)


		self.import_button = QPushButton('import')
		self.import_button.clicked.connect(self.import_with_json)
		self.import_layout.addWidget(self.import_button)

		self.importPath_label = QLabel('Path: ')
		self.import_layout.addWidget(self.importPath_label)

		self.importName_label = QLabel('Name: ')
		self.import_layout.addWidget(self.importName_label)

		self.replace_button = QPushButton('replace')
		self.replace_button.clicked.connect(self.replace_position)
		self.import_layout.addWidget(self.replace_button)

		self.import_grpbox.setLayout(self.import_layout)
		self.main_layout.addWidget(self.import_grpbox)
######## Method ##########
	def export_with_json(self):
		obj = mp.Position()
		#print obj.nameFile
		path = obj.export_position()

	def import_with_json(self):
		obj = mp.Position()
		self.filePath = QFileDialog.getOpenFileName()[0]
		self.importPath_label.setText('Path: %s'%self.filePath.split('/')[-1])
		self.json = obj.import_position(self.filePath)
		self.importName_label.setText('Name: %s'%self.json.keys())

	def replace_position(self):
		#print self.json
		obj = mp.Position()
		obj.replacePosition(self.filePath)


def main():
	global ui
	try:
		ui.close()
	except:
		pass
	ui = PositionGUI(parent = PTR)
	ui.show()

if __name__ == "__main__": #__name__ name of module
	main()  