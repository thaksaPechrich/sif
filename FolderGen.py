import os
class GenFolder(object):
	def __init__(self, root = '',project = ''):
		super(GenFolder, self).__init__()
		self.root = root
		if not self.checkInDir(self.root,project):
			os.mkdir(self.root+"\\"+project)
		self.root = self.root+"\\"+project
		self.path = self.root+"\\"+'work'
		self.step = []
		self.soft = []
		if not self.checkInDir(self.root,'work'):
			os.mkdir(self.root+"\\"+'work')
			os.mkdir(self.root+"\\"+'publish')

	def checkInDir(self,path,file):
		same = False
		for i in os.listdir(path):
			if i == file:
				same = True
		return same

	def genDirStep(self,step,path):
		self.step = step
		self.path = path
		for i in step:
			if not self.checkInDir(path,i):
				os.mkdir(path+'\\'+i)

	def genDirSoftware(self,step,soft):
		self.soft = soft
		for j in soft:
			if not self.checkInDir(self.path+'\\'+step,j):
				os.mkdir(self.path+'\\'+step+'\\'+j)

	def genInSoftware(self,step,soft):
		for i in soft:
			os.mkdir(self.path+'\\'+step+'\\'+i+'\\'+'scenes')
			os.mkdir(self.path+'\\'+step+'\\'+i+'\\'+'data')
			os.mkdir(self.path+'\\'+step+'\\'+i+'\\'+'images')
			os.mkdir(self.path+'\\'+step+'\\'+i+'\\'+'movies')

class GenFolAsset(GenFolder):
	def __init__(self, root, project):
		super(GenFolAsset, self).__init__(root, project)
		self.typ = []
		self.name = ''
		self.pathA = self.root+'\\'+'work'+'\\'+'assets'
		self.pathAP = self.root+'\\'+'publish'+'\\'+'assets'
		self.type = ['char','env','prop']
		self.step = ['Model','Rig','Texture','Lookdev','Dressing']
		self.stepPub = ['Cache','Rig','Texture','Material']
		if not self.checkInDir(self.root+'\\'+'work','assets'):
			os.mkdir(self.root+"\\"+'work'+'\\'+'assets')
			os.mkdir(self.root+"\\"+'publish'+'\\'+'assets')
			self.genType(self.type)

	def genType(self,typ):
		self.typ = typ
		for i in typ:
			if not self.checkInDir(self.pathA,i):
				os.mkdir(self.pathA+'\\'+i)
				os.mkdir(self.pathAP+'\\'+i)

	def genName(self,name,typ):
		self.name = name
		t = []
		t.append(typ)
		self.genType(t)
		if not self.checkInDir(self.pathA+'\\'+typ,name):
			os.mkdir(self.pathA+'\\'+typ+'\\'+name)
			path = self.pathA+'\\'+typ+'\\'+name
			self.genDirStep(self.step,path)
			for i in self.step:
				if i == 'Model':
					self.genDirSoftware(i,['maya','zbrush'])
					self.genInSoftware(i,['maya','zbrush'])
				elif i == 'Texture':
					self.genDirSoftware(i,['photoshop','substance'])
					self.genInSoftware(i,['photoshop','substance'])
				elif i == 'Lookdev':
					self.genDirSoftware(i,['maya'])
					self.genInSoftware(i,['maya'])
				elif i == 'Rig':
					self.genDirSoftware(i,['maya'])
					self.genInSoftware(i,['maya'])
				elif i == 'Dressing':
					self.genDirSoftware(i,['maya'])
					self.genInSoftware(i,['maya'])
		if not self.checkInDir(self.pathAP+'\\'+typ,name):
			os.mkdir(self.pathAP+'\\'+typ+'\\'+name)
			pathP = self.pathAP+'\\'+typ+'\\'+name
			self.genDirStep(self.stepPub,pathP)

	def genTexVer(self,name,typ,ver):
		version = 'v%03d'%ver
		if not self.checkInDir(self.pathAP+'\\'+typ+'\\'+name+'\\'+'Texture',version):
			os.mkdir(self.pathAP+'\\'+typ+'\\'+name+'\\'+'Texture'+'\\'+version)

class GenFolSeq(GenFolder):
	def __init__(self, root, project):
		super(GenFolSeq, self).__init__(root, project)
		self.seq = []
		self.shot = []
		self.step = ['Layout','Animation','ClothSim','Light','Composite']
		self.pathS = self.root+"\\"+'work'+'\\'+'sequences'
		self.pathSP = self.root+"\\"+'publish'+'\\'+'sequences'
		if not self.checkInDir(self.root+'\\'+'work','sequences'):
			os.mkdir(self.root+"\\"+'work'+'\\'+'sequences')
			os.mkdir(self.root+"\\"+'publish'+'\\'+'sequences')

	def genSeq(self,code):
		#Naming Folder seq??
		seqName = '%s'%code
		if not self.checkInDir(self.pathS,seqName):
			os.mkdir(self.pathS+'\\'+seqName)
			os.mkdir(self.pathSP+'\\'+seqName)

	def genShot(self,seqCode,code):
		#Naming Folder shot
		seqName = '%s'%seqCode
		shotName = '%s'%(code)
		if not self.checkInDir(self.pathS+'\\'+seqName,shotName):
			os.mkdir(self.pathS+'\\'+seqName+'\\'+shotName)
			path = self.pathS+'\\'+seqName+'\\'+shotName
			self.genDirStep(self.step,path)
			for i in self.step:
				if i == 'Layout':
					self.genDirSoftware(i,['maya'])
					self.genInSoftware(i,['maya'])
				elif i == 'Animation':
					self.genDirSoftware(i,['maya'])
					self.genInSoftware(i,['maya'])
				elif i == 'ColthSim':
					self.genDirSoftware(i,['maya'])
					self.genInSoftware(i,['maya'])
				elif i == 'Light':
					self.genDirSoftware(i,['maya'])
					self.genInSoftware(i,['maya'])
				elif i == 'Composite':
					self.genDirSoftware(i,['nuke'])
					self.genInSoftware(i,['nuke'])
		if not self.checkInDir(self.pathSP+'\\'+seqName,shotName):
			os.mkdir(self.pathSP+'\\'+seqName+'\\'+shotName)
			path = self.pathSP+'\\'+seqName+'\\'+shotName
			self.genDirStep(['Cache'],path)


if __name__ == '__main__':
	path = r"E:\SILPAKORN\intern\Intern_Project\HouseInForest"
	obj1 = GenFolAsset(path,'SIF')
#	obj1.genName('hashiv','char')
#	obj1.genName('ren','char')
#	obj1.genName('def','char')
#	obj1.genTexVer('ren','char',2)
#	obj1.genTexVer('ren','char',1)
	obj2 = GenFolSeq(path,'SIF')
#	obj2.genSeq('003')
#	obj2.genShot('003','001')
