import os
import maya.cmds as mc
import maya.OpenMayaUI as omui
import re

import manageShader as ms
reload(ms)

from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *
from shiboken2 import wrapInstance
from pprint import pprint 

PTR = wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)
PATH = os.path.split(__file__)[0].replace("\\","/")

class ShaderGUI(QDialog):
	def __init__(self,parent=''):
		super(ShaderGUI, self).__init__()
		self.filePath = ''
		self.json_data = {}
		self.resize(300,250)
		self.setWindowTitle('Manage Shader')
		self.main_layout = QVBoxLayout()
		self.setLayout(self.main_layout)
######## Export Shader ##########
		self.export_grpbox = QGroupBox('Export Shader: ')
		self.export_layout = QVBoxLayout()
		# self.export_layout.setAlignment(Qt.AlignTop)

		self.export_button = QPushButton('export shader and write json')
		self.export_button.clicked.connect(self.export_shader)
		self.export_layout.addWidget(self.export_button)

		self.export_grpbox.setLayout(self.export_layout)
		self.main_layout.addWidget(self.export_grpbox)
######## import Shader ##########
		self.import_grpbox = QGroupBox('Import Shader: ')
		self.import_layout = QVBoxLayout()
		self.import_layout.setAlignment(Qt.AlignTop)


		self.import_button = QPushButton('import shader')
		self.import_button.clicked.connect(self.import_shader)
		self.import_layout.addWidget(self.import_button)

		self.importName_label = QLabel('Shader Name: ')
		self.import_layout.addWidget(self.importName_label)

		self.replace_button = QPushButton('assign shader')
		self.replace_button.clicked.connect(self.replace_shader)
		self.import_layout.addWidget(self.replace_button)

		self.import_grpbox.setLayout(self.import_layout)
		self.main_layout.addWidget(self.import_grpbox)
######## Method ##########
	def export_shader(self):
		obj = ms.ManageShader()
		obj.writeJSON()
		obj.exportMaterial()

	def import_shader(self):
		obj = ms.ManageShader()
		self.filePath = QFileDialog.getOpenFileName()[0]
		self.json_data = obj.importMaterial(self.filePath)
		#print self.json_data.keys()
		self.importName_label.setText('Shader Name: %s'%self.json_data.keys())

	def replace_shader(self):
		obj = ms.ManageShader()
		obj.replaceMaterial(self.json_data)


		

def main():
	global ui
	try:
		ui.close()
	except:
		pass
	ui = ShaderGUI(parent = PTR)
	ui.show()

if __name__ == "__main__": #__name__ name of module
	main()  