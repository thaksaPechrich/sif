#shot  -startTime 1025 -endTime 1048 -sequenceStartTime 1 -sequenceEndTime 24 -currentCamera front -clip "" camera02;
#setAttr "camera02.sequenceStartFrame" 25;
#setAttr "camera02.track" 1;
#camera -centerOfInterest 5 -focalLength 35 -lensSqueezeRatio 1 -cameraScale 1 -horizontalFilmAperture 1.41732 -horizontalFilmOffset 0 -verticalFilmAperture 0.94488 -verticalFilmOffset 0 -filmFit Fill -overscan 1 -motionBlur 0 -shutterAngle 144 -nearClipPlane 0.1 -farClipPlane 10000 -orthographic 0 -orthographicWidth 30 -panZoomEnabled 0 -horizontalPan 0 -verticalPan 0 -zoom 1; objectMoveCommand; cameraMakeNode 1 "";
import maya.cmds as mc

class Camera(object):
	def __init__(self, name = '', frame = 0):
		super(Camera, self).__init__()
		self._name = name
		self._start_farme = 1001
		self._frame = frame-1
		self.end_frame = 1001

	@property
	def names(self):
		return self._name
	@names.setter
	def names(self, value):
		self._name = value

	@property
	def frame(self):
		return self._frame
	@frame.setter
	def frame(self, value):
		self._frame = value

	@property
	def start_frame(self):
		return self._start_farme
	@start_frame.setter
	def start_frame(self, value):
		self._start_farme = value

	def check_last_frame(self):
		self.end_frame = self.start_frame + self.frame
		return self.end_frame

	def check_start_frame(self):
		sels = mc.ls(type = 'camera')
		sel = mc.listRelatives(sels,parent = True)
		basicCam = ['front','persp','side','top','back','bottom','left']
		cam = []
		for i in sel:
			if not i in basicCam:
				cam.append(i)

		if len(cam) > 1:
			for i in cam:
				shot_name = '%s_SHOT'%i
				end = mc.shot(shot_name,q = True, et = True)
				if self.start_frame < end :
					self.start_frame = int(end+1)

	def create_camera(self):
		came = mc.camera()[0]
		mc.rename(came,self.names)

	def connect_shot(self):
		shot_name = '%s_SHOT'%self.names
		mc.shot(shot_name)
		self.check_start_frame()
		self.check_last_frame()
		print self.start_frame, self.end_frame
		mc.shot(shot_name, e = True, st = self.start_frame, et = self.end_frame,
									 sst = self.start_frame, set = self.end_frame)
		mc.shot(shot_name, e = True, cc = self.names)
		#setAttr "camera02.track" 1;
		mc.setAttr('%s.track'%shot_name, 1)

	




