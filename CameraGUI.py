import os
import maya.cmds as mc
import maya.OpenMayaUI as omui
import re

import createCamera as cc 
reload(cc)

from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *
from shiboken2 import wrapInstance

PTR = wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)
PATH = os.path.split(__file__)[0].replace("\\","/")

class CameraGUI(QDialog):
	def __init__(self,parent = ''):
		super(CameraGUI, self).__init__()
		self.resize(350,200)
		self.setWindowTitle('Create Camera')
		self.main_layout = QVBoxLayout()
		self.setLayout(self.main_layout)
		#self.main_layout.setAlignment(Qt.AlignTop)
		#self.main_layout.setAlignment(Qt.AlignHCenter)
		self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
		#=======================================#
		self.camera_grpbox = QGroupBox('Camera')
		self.camera_layout = QVBoxLayout()
		self.camera_layout.setAlignment(Qt.AlignTop)

		self.name_layout = QHBoxLayout()
		self.seq_combobox = QComboBox()
		self.getSuq()
		self.name_layout.addWidget(self.seq_combobox)

		self.name_linedit = QLineEdit()
		self.name_layout.addWidget(self.name_linedit)

		self.frame_label = QLabel('frame: ')
		self.name_layout.addWidget(self.frame_label)

		self.frame_linedit = QLineEdit()
		self.name_layout.addWidget(self.frame_linedit)

		self.name_widget = QWidget()
		self.name_widget.setLayout(self.name_layout)
		self.camera_layout.addWidget(self.name_widget)

		self.create_button = QPushButton('create')
		self.camera_layout.addWidget(self.create_button)
		self.create_button.clicked.connect(self.createButton)

		self.cameraname_label = QLabel('camera: [name]')
		self.camera_layout.addWidget(self.cameraname_label)

		self.framstrart_label = QLabel('start frame: ')
		self.frameend_label = QLabel('end frame: ')
		self.camera_layout.addWidget(self.framstrart_label)
		self.camera_layout.addWidget(self.frameend_label)

		self.camera_grpbox.setLayout(self.camera_layout)
		self.main_layout.addWidget(self.camera_grpbox)


	def lisInDir(self,path):
		item = []
		try:
			for i in os.listdir(path):
				item.append(i)
			return item
		except:
			return item

	def getSuq(self):
		pathFile = mc.file(q=True,exn=True)
		pathFile = re.split(r'(assets|sequences)',pathFile)[0]
		path = '%s/sequences'%pathFile
		self.seq_combobox.clear()
		item = self.lisInDir(path)
		for i in range(len(item)):
			self.seq_combobox.addItem(item[i])

	def createButton(self):
		seq = self.seq_combobox.currentText()
		name = self.name_linedit.text()
		nameCame = '%s_%s'%(seq,name)
		frame = int(self.frame_linedit.text())
		obj = cc.Camera(nameCame,frame)
		obj.create_camera()
		obj.connect_shot()
		self.cameraname_label.setText('camera: %s'%name)
		self.framstrart_label.setText('start frame: %s'%obj.start_frame)
		self.frameend_label.setText('end frame: %s'%obj.end_frame)






def main():
	global ui
	try:
		ui.close()
	except:
		pass
	ui = CameraGUI(parent = PTR)
	ui.show()

if __name__ == "__main__": #__name__ name of module
	main()  