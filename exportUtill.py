import maya.mel as mm
import maya.cmds as mc
import os
import re
import pprint
import stat
class FileUtil(object):
	def __init__(self,sel):
		super(FileUtil,self).__init__()
		self.fileP = mc.file(q=True,exn=True) # path+name.ma
		self.fileName = re.findall(r'\w+',self.fileP)[-2] #name #not .ma
		self._filePath = re.split(r'\w+_\w+_\w+_\w+_v[0-9]{3}.ma',self.fileP)[0] # path of file
		self._rootPath = re.split(r'(publish|work)',self.filePath)[0] # path of char/prop/env
		self.types = re.findall(r'(\w+)_\w+_\w+_\w+_v[0-9]{3}',self.fileName)[0] #type: char/prop/env
		self.name = re.findall(r'\w+_(\w+)_\w+_\w+_v[0-9]{3}',self.fileName)[0] #name og geometry
		self.step = re.findall(r'\w+_\w+_(\w+)_\w+_v[0-9]{3}',self.fileName)[0] #step: model/rig/tex/look
		self.task = re.findall(r'\w+_\w+_\w+_(\w+)_v[0-9]{3}',self.fileName)[0] #task: hi/lo/px
		self.version = re.findall(r'_v([0-9]{3})',self.fileName)[0] #version of file
		self.sel = sel

	@staticmethod
	def helpFile():
		helpF = 'def __str__(): return path of file \ndef __repr__():return path of file \ndef filePath(): return path of file\n 	can input new value Example: filePath = "C://work/scene" '		
		helpF += '\ndef rootPath(): return root path of file\n	can input new value Example: filePath = "C://work/scene" '
		helpF += '\ndef getFileName(): return name full of file'
		helpF += '\ndef getType(): return type of file'
		helpF += '\ndef getName(): return name of file'
		helpF += '\ndef getStep(): return step of file'
		helpF += '\ndef getTask(): return task of file'
		helpF += '\ndef getSel(): return name of object select'
		helpF += '\ndef getCurrentVer(): return version of file'
		helpF += '\ndef getNextVer(): return next versioin of file'
		helpF += '\ndef checkVersion(path,nameFile): return version and next version of file\n 	path: path of folder save file\n 	nameFile: name of file new save'
		helpF += '\ndef unReadOnly(path,name): unSetting file readOnly\n 	path: path of file\n 	name: name of file Example: workScript.py'
		helpF += '\ndef unReadOnly(path,name): setting file readOnly\n 	path: path of file\n 	name: name of file Example: workScript.py'
		print helpF


	def __str__(self):
		return self.filePath

	def __repr__(self):
		return self.filePath

	@property
	def filePath(self):
		filePath = self._filePath.replace('work','publish')
		filePath = filePath.split('Model')[0]
		self._filePath = filePath
		return self._filePath
	@filePath.setter
	def filePath(self, value):
		self._filePath = value

	@property
	def rootPath(self):
		return self._rootPath
	@rootPath.setter
	def rootPath(self, value):
		self._rootPath = value

	def getFileName(self):
		return self.fileName

	def getType(self):
		return self.types

	def getName(self):
		return self.name

	def getStep(self):
		return self.step

	def getTask(self):
		return self.task

	def getSel(self):
		return self.sel

	def getCurrentVer(self):
		return self.version()

	def getLastVer(self):
		lastVer = self.checkVersion(self.filePath,self.fileName)[1]
		return lastVer

	def getNextVer(self):
		nextVer = self.checkVersion(self.filePath,self.fileName)[0]
		return nextVer

	def checkVersion(self,path,nameFile):
		sameFile = []
		version = 0
		versionNew = 0
		if nameFile.split('_v'):
			nameFile = nameFile.split('_v')[0]
		for file in os.listdir(path): #for util in folder (path)
			if len(file.split('.')) > 1: #remove folder #check file can split py .(dot)
				if len(file.split('_v')) > 1:#check can split py _v
					name = re.findall(r'(\w+)_v[0-9]{3}',file)[0]
					if name == nameFile:
						sameFile.append(file.split('.')[0])
		for i in sameFile:
			if len(i.split('_v')) > 1 :
				version = int(i.split('_v')[1])
				if versionNew < version:
					versionNew = version
		return versionNew+1,version

	def unReadOnly(self,path,name):
		pathName = '%s%s'%(path,name)
		os.chmod(pathName, stat.S_IWUSR|stat.S_IWGRP|stat.S_IWOTH)

	def readOnly(self,path,name):
		pathName = '%s/%s'%(path,name)
		os.chmod(pathName, stat.S_IRUSR|stat.S_IRGRP|stat.S_IROTH)

class ExportObj(FileUtil):
	def __init__(self,sel, material = 0,smoothing = 1, normal = 1):
		super(ExportObj, self).__init__(sel)
		self.sel = sel
		self._material = material
		self._smoothing = smoothing
		self._normal = normal
		self.filePath = '%sCache/'%self.filePath

	def setFileName(self):
		self.fileName = self.fileName.replace('%s_%s_v'%(self.step,self.task),'OBJ_v')
		return self.fileName
		
	@property
	def material(self):
		return self._material
	@material.setter
	def material(self, value):
		self._material = value

	@property
	def smoothing(self):
		return self._smoothing
	@smoothing.setter
	def smoothing(self, value):
		self._smoothing = value

	@property
	def normal(self):
		return self._normal
	@normal.setter
	def normal(self, value):
		self._normal = value

	def exportFile(self):
		self.setFileName()
		self.fileName = self.fileName.replace(self.version,'%03d'%self.getNextVer())
#		print self.fileName,self.filePath
		op = 'file -op "groups=0;ptgroups=0;materials=%d;smoothing=%d;normals=%d"'%(self.material,self.smoothing,self.normal)
		typ = ' -typ "OBJexport" -pr '
		exportObj = r'-es "%s%s.obj";'%(self.filePath,self.fileName)
#		print op+typ+exportObj
		mm.eval(op+typ+exportObj)
#		self.readOnly(self.filePath,'%s.obj'%self.fileName)

class ExportAbc(FileUtil):
	def __init__(self, sel, stripNamespaces = True, uvWrite = True, worldSpace = True,
					   writeVisibility = True, start = 1, end = 1):
		super(ExportAbc, self).__init__(sel)
		self.sel = sel
		self._stripNamespaces = stripNamespaces
		self._uvWrite = uvWrite
		self._worldSpace = worldSpace
		self._writeVisibility = writeVisibility
		self._start = start
		self._end = end
		self.filePath = '%sCache'%self.filePath

	def setFileName(self):
		self.fileName = self.fileName.replace('%s_%s_v'%(self.step,self.task),'ABC_v')
		return self.fileName

	@staticmethod
	def helpAbc():
		helpA = 'def stripNamespaces(): return 0(False) or 1(True)\n	can input new value Example: stripNamespaces = 0 (for False) or 1 (for True)'
		helpA += '\ndef uvWrite(): return 0(False) or 1(True)\n	can input new value Example: uvWrite = 0 (for False) or 1 (for True)'
		helpA += '\ndef worldSpace(): return 0(False) or 1(True)\n	can input new value Example: worldSpace = 0 (for False) or 1 (for True)'
		helpA += '\ndef writeVisibility(): return 0(False) or 1(True)\n	can input new value Example: writeVisibility = 0 (for False) or 1 (for True)'
		helpA += '\ndef start(): return start frame\n	can input new value Example: start = 1'
		helpA += '\ndef end(): return end frame\n	can input new value Example: end = 1'
		helpA += '\ndef exportFile(): create file .abc'
		print helpA


	@property
	def stripNamespaces(self):
		return self._stripNamespaces
	@stripNamespaces.setter
	def stripNamespaces(self, value):
		self._stripNamespaces = value

	@property
	def uvWrite(self):
		return self._uvWrite
	@uvWrite.setter
	def uvWrite(self, value):
		self._uvWrite = value

	@property
	def worldSpace(self):
		return self._worldSpace
	@worldSpace.setter
	def worldSpace(self, value):
		self._worldSpace = value

	@property
	def writeVisibility(self):
		return self._writeVisibility
	@writeVisibility.setter
	def writeVisibility(self, value):
		self._writeVisibility = value

	@property
	def start(self):
		return self._start
	@start.setter
	def start(self, value):
		self._start = value

	@property
	def end(self):
		return self._end
	@end.setter
	def end(self, value):
		self._end = value

	def exportFile(self):
		self.setFileName()
		self.fileName = self.fileName.replace(self.version,'%03d'%self.getNextVer())
#		print self.fileName,self.filePath
		exportAbc = 'AbcExport -j "-frameRange %d %d '%(self.start,self.end)
		pathName = r'%s -file %s/%s.abc";'%(self.sel[0],self.filePath,self.fileName)
		op = ''		
		if self.stripNamespaces:
			op += '-stripNamespaces ' 
		if self.uvWrite:
			op += '-uvWrite '
		if self.worldSpace:
			op += '-worldSpace '
		if self.writeVisibility:
			op += '-writeVisibility '
		op += '-dataFormat ogawa -root |'
#		print exportAbc+op+pathName
		mm.eval(exportAbc+op+pathName)
		self.readOnly(self.filePath,'%s.abc'%self.fileName)

class ExportGpu(FileUtil):
	def __init__(self, sel, writeMaterials = True, start = 1, end = 1):
		super(ExportGpu, self).__init__(sel)
		self.sel = sel
		self._writeMaterials = writeMaterials
		self._start = start
		self._end = end
		self.filePath = '%sCache'%self.filePath

	def setFileName(self):
		self.fileName = self.fileName.replace('%s_%s_v'%(self.step,self.task),'GPU_v')
		return self.fileName

	@staticmethod
	def helpGpu():
		helpG = 'def writeMaterials(): return 0(False) or 1(True)\n	can input new value Example: writeMaterials = 0 (for False) or 1 (for True)'
		helpG += '\ndef start(): return start frame\n	can input new value Example: start = 1'
		helpG += '\ndef end(): return end frame\n	can input new value Example: end = 1'
		helpG += '\ndef exportFile(): create file .abc (GPU)'
		print helpG

	@property
	def writeMaterials(self):
		return self._writeMaterials
	@writeMaterials.setter
	def writeMaterials(self, value):
		self._writeMaterials = value

	@property
	def start(self):
		return self._start
	@start.setter
	def start(self, value):
		self._start = value

	@property
	def end(self):
		return self._end
	@end.setter
	def end(self, value):
		self._end = value

	def exportFile(self):
		self.setFileName()
		self.fileName = self.fileName.replace(self.version,'%03d'%self.getNextVer())
#		print self.fileName,self.filePath
		exportGpu = 'gpuCache '
		startEndTime = '-startTime %d -endTime %d '%(self.start,self.end)
		optimize = '-optimize -optimizationThreshold 40000'
		if self.writeMaterials:
			optimize += ' -writeMaterials'
		optimize += ' -dataFormat ogawa'

		directory = r' -directory "%s" -fileName "%s" %s;'%(self.filePath,self.fileName,self.sel[0])
#		print exportGpu+startEndTime+optimize+directory
		mm.eval(exportGpu+startEndTime+optimize+directory)
#		print '%s%s.abc'%(self.filePath,self.fileName)
		self.readOnly(self.filePath,'%s.abc'%self.fileName)

class exportMa(FileUtil):
	def __init__(self, sel):
		super(exportMa, self).__init__(sel)
		self.sel = sel
		self.filePath = '%sRig/'%self.filePath

	def setFileName(self):
		self.fileName = self.fileName.replace('model','rig')
		return self.fileName

	@staticmethod
	def helpMa():
		helpM = '\ndef exportFile(): create file .ma'
		print helpM

	def exportFile(self):
		self.setFileName()
		self.fileName = self.fileName.replace(self.version,'%03d'%self.getNextVer())
		print self.fileName,self.filePath
		mc.file(rename='%s%s.ma'%(self.filePath,self.fileName))
		mc.file(save=True, type="mayaAscii")
		self.readOnly(self.filePath,'%s.ma'%self.fileName)

		
