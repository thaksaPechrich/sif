import os
import maya.cmds as mc
import maya.OpenMayaUI as omui
import re

import playblastWithStam as ps
reload(ps)

from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *
from shiboken2 import wrapInstance
from pprint import pprint 

PTR = wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)
PATH = os.path.split(__file__)[0].replace("\\","/")

class PlayblastGUI(QDialog):
	def __init__(self,parent=''):
		super(PlayblastGUI, self).__init__()
		self.resize(300,180)
		self.setWindowTitle('Playblast Movie')
		self.main_layout = QVBoxLayout()
		self.setLayout(self.main_layout)
######## Export playblast ##########
		self.playblast_grpbox = QGroupBox('Playblast: ')
		self.playblast_layout = QVBoxLayout()
		self.playblast_layout.setAlignment(Qt.AlignTop)

		self.camera_layout = QHBoxLayout()
		self.camera_label = QLabel('camera: ')
		self.camera_layout.addWidget(self.camera_label)
		self.camera_widget = QWidget()
		self.camera_widget.setLayout(self.camera_layout)
		self.camera_combobox = QComboBox()
		self.camera_combobox.setMinimumContentsLength(25)
		self.get_camera()
		self.camera_layout.addWidget(self.camera_combobox)
		self.playblast_layout.addWidget(self.camera_widget)

		self.comment_layout = QHBoxLayout()
		self.comment_label = QLabel('comment: ')
		self.comment_layout.addWidget(self.comment_label)
		self.comment_lineeidt = QLineEdit()
		self.comment_layout.addWidget(self.comment_lineeidt)
		self.comment_widget = QWidget()
		self.comment_widget.setLayout(self.comment_layout)
		self.playblast_layout.addWidget(self.comment_widget)

		self.playblast_button = QPushButton('playblast')
		self.playblast_button.clicked.connect(self.playplast_movie)
		self.playblast_layout.addWidget(self.playblast_button)

		self.playblast_grpbox.setLayout(self.playblast_layout)
		self.main_layout.addWidget(self.playblast_grpbox)
######## Method playblast ##########
	def get_camera(self):
		self.camera_combobox.clear()
		cams = mc.ls(type = 'camera')
		not_came = ['persp','top','front','side','back', 'left','bottom','persp1']
		for cam in cams:
			tranform_cam = mc.listRelatives(cam, p=True)[0]
			if not tranform_cam in not_came:
				self.camera_combobox.addItem(tranform_cam)

	def playplast_movie(self):
		cam = self.camera_combobox.currentText()
		comment = self.comment_lineeidt.text()
		#print cam
		#print comment
		obj = ps.playblastWithStam()
		if comment == '':
			comment = 'None'
		obj.playblastStam(cam,comment)

def main():
	global ui
	try:
		ui.close()
	except:
		pass
	ui = PlayblastGUI(parent = PTR)
	ui.show()

if __name__ == "__main__": #__name__ name of module
	main()  