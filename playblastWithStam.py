import maya.cmds as mc
import getpass
import re
import subprocess
from datetime import datetime

class playblastWithStam(object):
	def __init__(self):
		super(playblastWithStam, self).__init__()
		self.path = self.fileP = mc.file(q=True,exn=True)
		self.nameFile = re.findall(r'\w+',self.fileP)[-2]
		self.actUser = getpass.getuser()
		self._start_frame = mc.playbackOptions(q=True, animationStartTime=True)
		self._end_frame = mc.playbackOptions(q=True, animationEndTime=True)

	@property
	def start_frame(self):
		return self._start_frame
	@start_frame.setter
	def start_frame(self, value):
		self._start_frame = value

	@property
	def end_frame(self):
		return self._end_frame
	@end_frame.setter
	def end_frame(self, value):
		self._end_frame = value

	def get_focal_length(self,cam):
		focal_length = mc.getAttr('%s.focalLength'%cam)
		return focal_length

	def assetImageDir(self,fol):
		self.path = self.path.split(self.nameFile)[0]
		self.path = self.path.replace('scenes',fol)
		return self.path

	def playblastStam(self,cam,comment=''):
		frame_cout = (self.end_frame - self.start_frame) + 1
		name = self.nameFile.split('.')[0]
		mc.lookThru(cam)
		moviePath = ''
		for frame in range(int(frame_cout)):
			start = int(self.start_frame+frame)
			end = int(self.start_frame+frame)
			#print '%s/%s_%d' % (self.assetImageDir(), name, start)
			moviePath = mc.playblast(startTime=start, 
						endTime=end,
						format='image',
						filename='%s/%s' % (self.assetImageDir('images'), name),
						sequenceTime=False,
						clearCache=True,
						viewer=False,
						showOrnaments=False,
						offScreen=True,
						fp=4,
						percent=100,
						compression='jpg',
						quality=100,
						widthHeight=[960, 540])
			namePath = '%s/%s.%d.jpg'%(self.assetImageDir('images'), name, start)
			self.stam_playblast(comment, namePath, end, cam)
		self.makeMovie(moviePath)

	def stam_playblast(self, comment, path,frame,cam):
		srcImg = path
		dstImg = path

		cmd = 'convert '
		cmd += srcImg
		###### artist #####
		cmd += ' -gravity southEast'
		cmd += ' -stroke #000000'
		cmd += ' -strokewidth 1'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +30+10 "Artist: %s" '%self.actUser
		cmd += ' -stroke none'
		cmd += ' -fill white'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +30+10 "Artist: %s" '%self.actUser
		##### frame #####
		cmd += ' -gravity northEast'
		cmd += ' -stroke #000000'
		cmd += ' -strokewidth 1'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +30+10 "%d/%d" '%(frame, self.end_frame)
		cmd += ' -stroke none'
		cmd += ' -fill white'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +30+10 "%d/%d" '%(frame,self.end_frame)
		##### focal lenght #####
		cmd += ' -gravity north'
		cmd += ' -stroke #000000'
		cmd += ' -strokewidth 1'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +0+10 "focal lenght: %.2f" '%self.get_focal_length(cam)
		cmd += ' -stroke none'
		cmd += ' -fill white'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +0+10 "focal lenght: %.2f" '%self.get_focal_length(cam)
		##### comment #####
		cmd += ' -gravity southWest'
		cmd += ' -stroke #000000'
		cmd += ' -strokewidth 1'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +20+30 "Comment: %s" '%comment
		cmd += ' -stroke none'
		cmd += ' -fill white'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +20+30 "Comment: %s" '%comment
		##### file name #####
		cmd += ' -gravity southWest'
		cmd += ' -stroke #000000'
		cmd += ' -strokewidth 1'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +20+10 "%s" '%self.nameFile
		cmd += ' -stroke none'
		cmd += ' -fill white'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +20+10 "%s" '%self.nameFile	
		##### date time #####
		cmd += ' -gravity northWest'
		cmd += ' -stroke #000000'
		cmd += ' -strokewidth 1'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +20+10 "{}" '.format(datetime.now())
		cmd += ' -stroke none'
		cmd += ' -fill white'
		cmd += ' -pointsize 13'
		cmd += ' -annotate +20+10 "{}" '.format(datetime.now())
		
		cmd += dstImg

		#print cmd 
		subprocess.check_output(cmd, shell=True)

	def makeMovie(self,path):
		outPath = '%s%s.mp4'%(self.assetImageDir('movies'),self.nameFile.split('.')[0])
		outPath = outPath.replace('images','movies')
		wordReplace = re.findall(r'\w+_v[0-9]{3}.(\W+).\w+',path)[0]
		expression = re.findall(r'\w+_v[0-9]{3}.\W+(.\w+)',path)[0]
		path = path.replace(wordReplace,r'%04d')
		path = path.replace(expression,'.%s'%expression)
		outPath = outPath.replace('/','\\')
		path = path.replace('/','\\')
#		print path
#		print outPath
#ffmpeg -start_number 1001 -t 3 -i E:\SILPAKORN\intern\Intern_Project\HouseInForest\SIF\work\sequences\AAA\sh0020\Animation\maya\images\AAA_sh0020_animation_blocking_v002.%04d.jpg -r 24 -vcodec libx264 -qscale:v 1 C:\Users\com\Desktop\test.mp4
		cmd = 'ffmpeg '
		cmd += '-start_number %s '%self.start_frame
		cmd += '-i %s '%path
		cmd += '-r 24 '
		cmd += '-vcodec libx264 '
		cmd += '-qscale:v 1 '
		cmd += outPath		
		subprocess.check_output(cmd, shell=True)
		print cmd

		
