import os
import sys
import re
import maya.cmds as mc

class FileUtil(object):
	def __init__(self, path, typ, name, step, task, ver = 1):
		super(FileUtil, self).__init__()
		self.path = path
		self._typ = typ
		self._name = name
		self._step = step
		self._task = task
		self._ver = ver

	def __str__(self):
		return self.path

	def __repr__(self):
		return self.path

###### Set/Get ######
	@property
	def typ(self):
		return self._typ
	@typ.setter
	def typ(self, value):
		self._typ = value

	@property
	def name(self):
		return self._name
	@name.setter
	def name(self, value):
		self._name = value

	@property
	def step(self):
		return self._step
	@step.setter
	def step(self, value):
		self._step = value

	@property
	def task(self):
		return self._task
	@task.setter
	def task(self, value):
		self._task = value

	@property
	def ver(self):
		return self._ver
	@ver.setter
	def ver(self, value):
		self._ver = value

	def getNextVer(self):
		self.nextVer = self.checkNextVersion()
		return self.nextVer

###### Method in Class ######
	def checkNextVersion(self):
		nameFile = '%s_%s_%s_%s_v%03d'%(self.typ,self.name,self.step,self.task,self.ver)
		sameFile = []
		version = 0
		versionNew = 0
		if nameFile.split('_v'):
			nameFile = nameFile.split('_v')[0]
		for file in os.listdir(self.path): #for util in folder (path)
			if len(file.split('.')) > 1: #remove folder #check file can split py .(dot)
				if len(file.split('_v')) > 1:#check can split py _v
					name = re.findall(r'(\w+)_v[0-9]{3}',file)[0]
					if name == nameFile:
						sameFile.append(file.split('.')[0])
		for i in sameFile:
			if len(i.split('_v')) > 1 :
				version = int(i.split('_v')[1])
				if versionNew < version:
					versionNew = version
		return versionNew+1

	def saveFile(self):
		ver = self.getNextVer()
		name = '%s_%s_%s_%s_v%03d'%(self.typ,self.name,self.step,self.task,ver)

		mc.file(rename='%s/%s.ma'%(self.path,name))
		mc.file(save=True, type="mayaAscii")


if __name__ == '__main__':
	path = r"E:\SILPAKORN\intern\Intern_Project\HouseInForest"
	obj1 = FileUtil(path,'char','hashiv','model','hi')
	obj1.getNextVer()
#	obj1.checkNextVersion()
