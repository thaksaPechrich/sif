try:
	from PySide.QtGui import *
	from PySide.QtCore import *
except:
	from PySide2.QtWidgets import *
	from PySide2.QtGui import *
	from PySide2.QtCore import *

import FolderGen as fg
reload(fg)

import sys
import os

PATH = os.path.split(__file__)[0].replace("\\","/")

class GenFolGUI(QDialog):
	def __init__(self, parent=None):
		super(GenFolGUI, self).__init__(parent)
		self.main_layout = QVBoxLayout()
		self.main_layout.setAlignment(Qt.AlignTop)
		self.setLayout(self.main_layout)
		self.resize(490, 400)
		self.setWindowTitle('Directory Generate tool')
		self.pro_label = QLabel('Project')
		self.main_layout.addWidget(self.pro_label)
		self.icon_path = '%s/icon/folder.png'%PATH
		self.setWindowIcon(QIcon(self.icon_path))
		
######## Direct Projec ########
		self.pro_layout = QVBoxLayout()
		self.dirPro_layout = QHBoxLayout()
		self.dirPro_label = QLabel('Direct Project: ')
		self.dirPro_editline = QLineEdit()
		self.dirPro_button = QPushButton('Browse',self)
		self.dirPro_button.clicked.connect(self.dirBrows)

		self.dirPro_layout.addWidget(self.dirPro_label)
		self.dirPro_layout.addWidget(self.dirPro_editline)
		self.dirPro_layout.addWidget(self.dirPro_button)

		self.dirPro_widget = QWidget()
		self.dirPro_widget.setLayout(self.dirPro_layout)
		self.pro_layout.addWidget(self.dirPro_widget)

		#----------------------------#
		self.proName_layout = QHBoxLayout()
		self.proName_label = QLabel('Project Name: ')
		self.proName_editline = QLineEdit()
		self.proName_button = QPushButton('create',self)
		self.proName_button.clicked.connect(self.genProject)
		self.proName_bbutton = QPushButton('Browse')
		self.proName_bbutton.clicked.connect(self.proNameBrows)

		self.proName_layout.addWidget(self.proName_label)
		self.proName_layout.addWidget(self.proName_editline)
		self.proName_layout.addWidget(self.proName_button)
		self.proName_layout.addWidget(self.proName_bbutton)

		self.proName_widget = QWidget()
		self.proName_widget.setLayout(self.proName_layout)
		self.pro_layout.addWidget(self.proName_widget)
		#----------Frame-------------#
		self.dirPro_frame = QFrame()
		self.dirPro_frame.setFrameStyle(QFrame.Panel | QFrame.Plain)
		self.dirPro_frame.setLayout(self.pro_layout)
		self.main_layout.addWidget(self.dirPro_frame)

######## Assets ########
		self.asset_label = QLabel('Assets')
		self.main_layout.addWidget(self.asset_label)

		self.asset_layout = QHBoxLayout()

		self.type_combobox = QComboBox()
		self.type_combobox.addItem('char')
		self.type_combobox.addItem('env')
		self.type_combobox.addItem('prop')

		self.asset_label = QLabel('Asset Name: ')
		self.asset_editline = QLineEdit()
		self.asset_button = QPushButton('create',self)
		self.asset_button.clicked.connect(self.genAsset)

		self.asset_layout.addWidget(self.type_combobox)
		self.asset_layout.addWidget(self.asset_label)
		self.asset_layout.addWidget(self.asset_editline)
		self.asset_layout.addWidget(self.asset_button)
		#----------Frame------------#
		self.asset_frame = QFrame()
		self.asset_frame.setFrameStyle(QFrame.Panel | QFrame.Plain)
		self.asset_frame.setLayout(self.asset_layout)
		self.main_layout.addWidget(self.asset_frame)

######## Sequences ########
		self.seq_label = QLabel('Sequences')
		self.main_layout.addWidget(self.seq_label)

		self.seq_layout = QHBoxLayout()
		self.seq_label = QLabel('Sequences Name: ')
		self.seq_editline = QLineEdit()
		self.seq_button = QPushButton('create',self)
		self.seq_button.clicked.connect(self.genSeq)

		self.seq_layout.addWidget(self.seq_label)
		self.seq_layout.addWidget(self.seq_editline)
		self.seq_layout.addWidget(self.seq_button)

		self.seq_widget = QWidget()
		self.seq_widget.setLayout(self.seq_layout)
		#----------Shot------------#
		self.shot_layout = QHBoxLayout()
		self.shot_layout.setAlignment(Qt.AlignTop)
		self.seqLis_layout = QVBoxLayout()

		self.seq_listWidget = QListWidget()
		self.size_icon = QSize(15,15)
		#-----------------------------#
		self.seqLis_label = QLabel('select seq:')

		self.seq_listWidget.setIconSize(self.size_icon)
		self.seq_listWidget.setResizeMode(QListWidget.Adjust)
		self.seq_listWidget.setMovement(QListView.Static)
	
		self.seqLis_layout.addWidget(self.seqLis_label)
		self.seqLis_layout.addWidget(self.seq_listWidget)

		self.seqLis_widget = QWidget()
		self.seqLis_widget.setLayout(self.seqLis_layout)
		self.shot_layout.addWidget(self.seqLis_widget)
		#-----------------------------#
		
		self.shotN_layout = QVBoxLayout()
		self.shotN_layout.setAlignment(Qt.AlignTop)
		self.shotName_layout = QHBoxLayout()
		self.shotName_label = QLabel('Shot Name:')
		self.shotName_linEdit = QLineEdit()

		self.shotName_layout.addWidget(self.shotName_label)
		self.shotName_layout.addWidget(self.shotName_linEdit)

		self.shotName_widget = QWidget()
		self.shotName_widget.setLayout(self.shotName_layout)
		self.shotN_layout.addWidget(self.shotName_widget)
		self.shotName_button = QPushButton('create')
		self.shotName_button.clicked.connect(self.genShot)

		self.shotN_layout.addWidget(self.shotName_button)

		self.shotN_widget = QWidget()
		self.shotN_widget.setLayout(self.shotN_layout)
		self.shot_layout.addWidget(self.shotN_widget)

		#------------------#
		self.shot_widget = QWidget()
		self.shot_widget.setLayout(self.shot_layout)

		self.seqSum_layout = QVBoxLayout()
		self.seqSum_layout.addWidget(self.seq_widget)
		self.seqSum_layout.addWidget(self.shot_widget)
		
		#----------Frame------------#
		self.seq_frame = QFrame()
		self.seq_frame.setFrameStyle(QFrame.Panel | QFrame.Plain)
		self.seq_frame.setLayout(self.seqSum_layout)
		self.main_layout.addWidget(self.seq_frame)

######## Connect Func ########
	def lisInDir(self,path):
		item = []
		try:
			for i in os.listdir(path):
				item.append(i)
			return item
		except:
			return item

	def dirBrows(self):
		fileDir = QFileDialog.getExistingDirectory()
		self.dirPro_editline.setText(fileDir)

	def proNameBrows(self):
		nameProDir = QFileDialog.getExistingDirectory()
		namePro = nameProDir.split('/')[-1]
		self.proName_editline.setText(namePro)
		fileDir = nameProDir.split('/%s'%namePro)[0]
		self.dirPro_editline.setText(fileDir)

		self.seq_listWidget.clear()
		self.asset_editline.clear()
		self.seq_editline.clear()
		self.shotName_linEdit.clear()

		item = self.lisInDir(r'%s\work\sequences'%nameProDir)
		iconPath = r"%s/icon/folder.png"%PATH
		self.pic_icon = QIcon(iconPath)		
		for i in range(len(item)):
			itemList = 'self.pic%02d_widgetItem'%i
			itemList = QListWidgetItem()
			itemList.setIcon(self.pic_icon)
			itemList.setText(item[i])
			self.seq_listWidget.addItem(itemList)

	def pathSeq(self):
		path = ''

	def genProject(self):
		root = self.dirPro_editline.text()
		namePro = self.proName_editline.text()
		obj1 = fg.GenFolAsset(root,namePro)
		obj2 = fg.GenFolSeq(root,namePro)
		self.seq_listWidget.clear() 

	def genAsset(self):
		root = self.dirPro_editline.text()
		namePro = self.proName_editline.text()
		nameAsset = self.asset_editline.text()
		typ = self.type_combobox.currentText()
		obj1 = fg.GenFolAsset(root,namePro)
		obj1.genName(nameAsset,typ) 

	def genSeq(self):
		root = self.dirPro_editline.text()
		namePro = self.proName_editline.text()
		nameSeq = self.seq_editline.text()
		obj1 = fg.GenFolSeq(root,namePro)
		obj1.genSeq(nameSeq)

		self.seq_listWidget.clear() 
		item = self.lisInDir(r'%s/%s\work\sequences'%(root,namePro))
		iconPath = r"%s/icon/folder.png"%PATH
		self.pic_icon = QIcon(iconPath)		
		for i in range(len(item)):
			itemList = 'self.pic%02d_widgetItem'%i
			itemList = QListWidgetItem()
			itemList.setIcon(self.pic_icon)
			itemList.setText(item[i])
			self.seq_listWidget.addItem(itemList)

	def genShot(self):
		root = self.dirPro_editline.text()
		namePro = self.proName_editline.text()
		seq = self.seq_listWidget.currentItem().text()
		shotName = self.shotName_linEdit.text()
		obj1 = fg.GenFolSeq(root,namePro)
		obj1.genShot(seq,shotName)

def main():
	if not sys.argv[0].endswith('maya.exe'):
		app = QApplication(sys.argv)

		myDialog = GenFolGUI()
		myDialog.show()

		sys.exit(app.exec_())

	else:
		from shiboken2 import wrapInstance
		import maya.OpenMayaUI as omui

		global maya_myDialog

		ptr = wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)

		try:
			maya_myDialog.close()
		except:
			pass

		maya_myDialog = GenFolGUI(parent=ptr)
		maya_myDialog.show()


if __name__ == '__main__':
	main()