try:
	from PySide.QtGui import *
	from PySide.QtCore import *
except:
	from PySide2.QtWidgets import *
	from PySide2.QtGui import *
	from PySide2.QtCore import *

import saveFile as sf
reload(sf)
import FolderGen as gf
reload(gf)
import exportUtill as eu
reload(eu)
import splitShot as ss
reload(ss)
import manageStandIn as ms
reload(ms)

import sys
import os
import maya.cmds as mc

PATH = os.path.split(__file__)[0].replace("\\","/")

class saveFileGUI(QDialog):
	"""docstring for saveFileGUI"""
	def __init__(self,parent=None):
		super(saveFileGUI, self).__init__()
		self.main_layout = QVBoxLayout()
		self.main_layout.setAlignment(Qt.AlignTop)

		self.setLayout(self.main_layout)
		self.resize(480, 550)
		self.setWindowTitle('Scenes Management')
		#self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)		

######## Direct Projec ########
		self.dirPro_layout = QHBoxLayout()
		self.dirPro_label = QLabel('Direct Project: ')
		self.dirPro_editline = QLineEdit()
		self.dirPro_button = QPushButton('Browse',self)
		self.dirPro_button.clicked.connect(self.dirBrows)

		self.dirPro_layout.addWidget(self.dirPro_label)
		self.dirPro_layout.addWidget(self.dirPro_editline)
		self.dirPro_layout.addWidget(self.dirPro_button)

		self.dirPro_widget = QWidget()
		self.dirPro_widget.setLayout(self.dirPro_layout)
		self.main_layout.addWidget(self.dirPro_widget)
		self.icon_path = '%s/icon/destroyed-planet.ico'%PATH
		self.setWindowIcon(QIcon(self.icon_path))

######## TAP Bar ########
		self.pro_tapWidget = QTabWidget()
		self.asset_tap= QWidget()
		self.seq_tap= QWidget()
#================ Asset Tap ==================#
		#======= Layout in Tap save asset =======#
		self.save_layout = QVBoxLayout()
		self.save_layout.setAlignment(Qt.AlignCenter)

		#======= Layout in Tap save asset =======#
		self.save_gropBox = QGroupBox('Save Asset:')

		self.saveBox_layout = QVBoxLayout()
		self.saveBox_layout.setAlignment(Qt.AlignTop)

		self.nameSave_layout = QHBoxLayout()

		self.type_combobox = QComboBox()
		self.type_combobox.addItem('char')
		self.type_combobox.addItem('env')
		self.type_combobox.addItem('prop')
		self.nameSave_layout.addWidget(self.type_combobox)
		self.type_combobox.currentTextChanged.connect(lambda : self.find('asset'))

		self.name_combobox = QComboBox()

		self.nameSave_layout.addWidget(self.name_combobox)
		self.type_combobox.currentTextChanged.connect(self.getName) 

		self.type_combobox.currentTextChanged.connect(lambda : self.find('asset'))
		self.name_combobox.currentTextChanged.connect(lambda : self.find('asset'))

		self.step_combobox = QComboBox()
		self.step_combobox.addItem('model')
		self.step_combobox.addItem('rig')
		self.step_combobox.addItem('texture')
		self.step_combobox.addItem('lookdev')
		self.step_combobox.addItem('dressing')
		self.nameSave_layout.addWidget(self.step_combobox)

		self.task_combobox = QComboBox()
		self.nameSave_layout.addWidget(self.task_combobox)
		self.task_combobox.addItem('hi')
		self.step_combobox.currentTextChanged.connect(self.getTask)

		self.step_combobox.currentTextChanged.connect(lambda : self.find('asset')) 
		self.task_combobox.currentTextChanged.connect(lambda : self.find('asset'))       

		self.nameSave_widget = QWidget()
		self.nameSave_widget.setLayout(self.nameSave_layout)
		self.saveBox_layout.addWidget(self.nameSave_widget)

		self.save_button = QPushButton('save',self)
		self.save_button.clicked.connect(self.saveFile)
		self.saveBox_layout.addWidget(self.save_button)

		self.fileSave_lisWidget = QListWidget()
		self.fileSave_lisWidget.setFixedSize(360,100)
		self.size_icon = QSize(15,15)
		self.fileSave_lisWidget.setIconSize(self.size_icon)
		self.fileSave_lisWidget.setResizeMode(QListWidget.Adjust)
		self.fileSave_lisWidget.setMovement(QListView.Static)
		self.saveBox_layout.addWidget(self.fileSave_lisWidget)

		self.open_button = QPushButton('open',self)
		self.open_button.clicked.connect(lambda : self.open('asset'))
		self.saveBox_layout.addWidget(self.open_button)


		#-----------------------------

		self.save_gropBox.setLayout(self.saveBox_layout)
		self.save_layout.addWidget(self.save_gropBox)

		#======= Layout in Tap export asset =======#
		self.export_gropBox = QGroupBox('Export Asset:')
		self.export_layout = QVBoxLayout()  

		self.checkbox_layout = QHBoxLayout()
		self.abc_checkBox = QCheckBox('.abc',self)
		self.abc_checkBox.setCheckState(Qt.Checked)
		self.gpu_checkBox = QCheckBox('.gpu',self)
		self.gpu_checkBox.setCheckState(Qt.Checked)
		self.ma_checkBox = QCheckBox('.ma',self)
		self.obj_checkBox = QCheckBox('.obj',self)

		self.checkbox_layout.addWidget(self.abc_checkBox)
		self.checkbox_layout.addWidget(self.gpu_checkBox)
		self.checkbox_layout.addWidget(self.ma_checkBox)
		self.checkbox_layout.addWidget(self.obj_checkBox)

		self.checkbox_widget = QWidget()
		self.checkbox_widget.setLayout(self.checkbox_layout)
		self.export_layout.addWidget(self.checkbox_widget)

		self.option_gropBox = QGroupBox('Option')
		self.option_layout = QHBoxLayout()

		self.abc_layout = QVBoxLayout()
		self.abc_groupBox = QGroupBox('ABC')
		self.stripNamespaces_checkbox = QCheckBox('stripNamspaces',self)
		self.stripNamespaces_checkbox.setCheckState(Qt.Checked)
		self.uvWrite_checkbox = QCheckBox('uvWrite',self)
		self.uvWrite_checkbox.setCheckState(Qt.Checked)
		self.worldSpace_checkbox = QCheckBox('worldSpace',self)
		self.worldSpace_checkbox.setCheckState(Qt.Checked)
		self.writeVisibility_checkbox = QCheckBox('writeVisibility',self)
		self.writeVisibility_checkbox.setCheckState(Qt.Checked)

		self.abc_layout.addWidget(self.stripNamespaces_checkbox)
		self.abc_layout.addWidget(self.uvWrite_checkbox)
		self.abc_layout.addWidget(self.worldSpace_checkbox)
		self.abc_layout.addWidget(self.writeVisibility_checkbox)

		self.abc_groupBox.setLayout(self.abc_layout)
		self.option_layout.addWidget(self.abc_groupBox)

		self.gpu_layout = QVBoxLayout()
		self.gpu_groupBox = QGroupBox('GPU')

		self.writeMaterials_checkbox = QCheckBox('writeMaterials',self)
		self.writeMaterials_checkbox.setCheckState(Qt.Checked)

		self.gpu_layout.addWidget(self.writeMaterials_checkbox)

		self.gpu_groupBox.setLayout(self.gpu_layout)
		self.option_layout.addWidget(self.gpu_groupBox)

		#--------------------------------#
		#start and end frame
		self.start_layout = QHBoxLayout()
		self.start_layout.setAlignment(Qt.AlignTop)
		self.start_label = QLabel('start Frame: ')
		self.start_lineEdit = QLineEdit('1')

		self.start_layout.addWidget(self.start_label)
		self.start_layout.addWidget(self.start_lineEdit)

		self.start_widget = QWidget()
		self.start_widget.setLayout(self.start_layout)
		self.gpu_layout.addWidget(self.start_widget)


		self.end_layout = QHBoxLayout()
		self.end_layout.setAlignment(Qt.AlignTop)
		self.end_label = QLabel('end Frame: ')
		self.end_lineEdit = QLineEdit('1')

		self.end_layout.addWidget(self.end_label)
		self.end_layout.addWidget(self.end_lineEdit)

		self.end_widget = QWidget()
		self.end_widget.setLayout(self.end_layout)
		self.gpu_layout.addWidget(self.end_widget)

		self.export_button = QPushButton('export',self)
		self.export_button.clicked.connect(self.export)

		self.option_gropBox.setLayout(self.option_layout)
		self.export_layout.addWidget(self.option_gropBox)
		self.export_layout.addWidget(self.export_button)

		#********** Layout for export ********#
		self.export_gropBox.setLayout(self.export_layout)
		self.exportBox_layout = QVBoxLayout()
		self.exportBox_layout.setAlignment(Qt.AlignTop)

		#-----------------------------#
		self.export_gropBox.setLayout(self.exportBox_layout)
		self.save_layout.addWidget(self.export_gropBox)

		#********** StandIN Group ********#
		self.stanin_grpbox = QGroupBox('StandIN')
		self.stanin_layout = QVBoxLayout()

		#export standIN
		self.staninExport_grpbox = QGroupBox('export')
		self.staninExport_layout = QVBoxLayout()

		self.staninExport_button = QPushButton('export StandIN')
		self.staninExport_button.clicked.connect(self.export_standin)
		self.staninExport_layout.addWidget(self.staninExport_button)

		self.staninExport_grpbox.setLayout(self.staninExport_layout)
		self.stanin_layout.addWidget(self.staninExport_grpbox)

		#import standIN
		self.staninImport_grpbox = QGroupBox('import')
		self.staninImport_layout = QVBoxLayout()

		self.stnImportName_layout = QHBoxLayout()
		self.stnImportType_combobox = QComboBox()
		self.stnImportType_combobox.addItem('char')
		self.stnImportType_combobox.addItem('env')
		self.stnImportType_combobox.addItem('prop')
		self.stnImportName_layout.addWidget(self.stnImportType_combobox)

		
		self.stnImportName_combobox = QComboBox()
		self.stnImportType_combobox.currentTextChanged.connect(self.getNameStandIn)

		self.stnImportName_layout.addWidget(self.stnImportName_combobox)

		self.stnImportName_widget = QWidget()
		self.stnImportName_widget.setLayout(self.stnImportName_layout)
		self.staninImport_layout.addWidget(self.stnImportName_widget)


		self.staninImport_button = QPushButton('Import StandIN')
		self.staninImport_button.clicked.connect(self.import_standin)
		self.staninImport_layout.addWidget(self.staninImport_button)

		self.staninImport_grpbox.setLayout(self.staninImport_layout)
		self.stanin_layout.addWidget(self.staninImport_grpbox)

		self.stanin_grpbox.setLayout(self.stanin_layout)
		self.save_layout.addWidget(self.stanin_grpbox)


		#----------- Scrollbar -------------#
		widget = QWidget()
		widget.setLayout(self.save_layout)

		scroll = QScrollArea()
		scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
		scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		scroll.setWidgetResizable(False)
		scroll.setWidget(widget)
		scroll.setAlignment(Qt.AlignHCenter)

		layout = QVBoxLayout()
		layout.addWidget(scroll)    
		self.asset_tap.setLayout(layout)

#======================= Seq Tap =========================#
		self.seq_layout = QVBoxLayout()
		self.seq_grpbox= QGroupBox('Save Shot')

		self.seqGrp_layout = QVBoxLayout()
		self.seqGrp_layout.setAlignment(Qt.AlignTop)
		#-------- Shot name save --------#
		self.saveshot_layout = QHBoxLayout()
		self.seq_combobox = QComboBox()
		self.saveshot_layout.addWidget(self.seq_combobox)

		self.shot_combobox = QComboBox()
		self.seq_combobox.currentTextChanged.connect(self.getShot) 
		self.saveshot_layout.addWidget(self.shot_combobox)  

		self.seq_combobox.currentTextChanged.connect(lambda : self.find('shot'))
		self.shot_combobox.currentTextChanged.connect(lambda : self.find('shot'))

		self.stepSeq_combobox = QComboBox()
		self.stepSeq_combobox.addItem('layout')
		self.stepSeq_combobox.addItem('animation')
		self.stepSeq_combobox.addItem('clothSim')
		self.stepSeq_combobox.addItem('light')
		self.stepSeq_combobox.addItem('composite')
		self.saveshot_layout.addWidget(self.stepSeq_combobox)  

		self.taskSeq_combobox = QComboBox()
		self.taskSeq_combobox.addItem('master')
		self.stepSeq_combobox.currentTextChanged.connect(self.getTaskSeq) 
		self.saveshot_layout.addWidget(self.taskSeq_combobox)

		self.stepSeq_combobox.currentTextChanged.connect(lambda : self.find('shot'))
		self.taskSeq_combobox.currentTextChanged.connect(lambda : self.find('shot'))

		self.saveshot_widget = QWidget()
		self.saveshot_widget.setLayout(self.saveshot_layout)
		self.seqGrp_layout.addWidget(self.saveshot_widget)

		self.saveshot_button = QPushButton('save')
		self.saveshot_button.clicked.connect(self.saveShot)
		self.seqGrp_layout.addWidget(self.saveshot_button)

		self.shotSave_lisWidget = QListWidget()
		self.shotSave_lisWidget.setFixedSize(380,100)
		self.size_icon = QSize(15,15)
		self.shotSave_lisWidget.setIconSize(self.size_icon)
		self.shotSave_lisWidget.setResizeMode(QListWidget.Adjust)
		self.shotSave_lisWidget.setMovement(QListView.Static)
		self.seqGrp_layout.addWidget(self.shotSave_lisWidget)

		self.openshot_button = QPushButton('open')
		self.openshot_button.clicked.connect(lambda : self.open('shot'))
		self.seqGrp_layout.addWidget(self.openshot_button)

		#-------- Export shot --------#

		self.shotExport_grpbox = QGroupBox('Split Shot')
		self.shotExport_layout = QVBoxLayout()

		self.split_button = QPushButton('split')
		self.split_button.clicked.connect(self.split)
		self.shotExport_layout.addWidget(self.split_button)


		#----------------#

		self.seq_grpbox.setLayout(self.seqGrp_layout)
		self.seq_layout.addWidget(self.seq_grpbox)

		self.shotExport_grpbox.setLayout(self.shotExport_layout)
		self.seq_layout.addWidget(self.shotExport_grpbox)

		#self.seq_tap.setLayout(self.seq_layout)

		#----------- Scrollbar -------------#
		widgetSeq = QWidget()
		widgetSeq.setLayout(self.seq_layout)

		scrollSeq = QScrollArea()
		scrollSeq.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
		scrollSeq.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		scrollSeq.setWidgetResizable(False)
		scrollSeq.setWidget(widgetSeq)
		scrollSeq.setAlignment(Qt.AlignHCenter)

		layoutSep = QVBoxLayout()
		layoutSep.setAlignment(Qt.AlignCenter)
		layoutSep.addWidget(scrollSeq)    
		self.seq_tap.setLayout(layoutSep)

		#===========Add Tap===========#
		self.pro_tapWidget.addTab(self.asset_tap,'Assets')
		self.pro_tapWidget.addTab(self.seq_tap,'Sequences')

		self.main_layout.addWidget(self.pro_tapWidget)

################# Method Class ############

	def getTask(self):
		if self.step_combobox.currentText() == 'model':
			step = 'model'
			task = ['hi']
		elif self.step_combobox.currentText() == 'texture':
			step = 'texture'
			task = ['lo','hi']
		elif self.step_combobox.currentText() == 'lookdev':
			step = 'lookdev'
			task = ['hi']
		elif self.step_combobox.currentText() == 'rig':
			step = 'rig'
			task = ['hi']
		elif self.step_combobox.currentText() == 'dressing':
			step = 'dressing'
			task = ['hi']
			
		self.task_combobox.clear()  
		for i in range(len(task)):
			self.task_combobox.addItem(task[i])

	def getTaskSeq(self):
		step = self.stepSeq_combobox.currentText()
		if step == 'layout' or step == 'light' or step == 'composite':
			task = ['master']
		elif step == 'animation':
			task = ['blocking','polish']
		elif step == 'clothSim':
			task = ['blanket']

		self.taskSeq_combobox.clear()  
		for i in range(len(task)):
			self.taskSeq_combobox.addItem(task[i])
	def getNameStandIn(self):
		typ = self.stnImportType_combobox.currentText()
		self.stnImportName_combobox.clear()  
		if typ == 'env':
			self.stnImportName_combobox.addItem('treeA')
			self.stnImportName_combobox.addItem('treeB')
			self.stnImportName_combobox.addItem('treeC')
			self.stnImportName_combobox.addItem('treeD')
			self.stnImportName_combobox.addItem('treeF')
		elif typ == 'prop':
			self.stnImportName_combobox.addItem('bed')
			self.stnImportName_combobox.addItem('pillow')
			self.stnImportName_combobox.addItem('table')
			self.stnImportName_combobox.addItem('window')

	def lisInDir(self,path):
		item = []
		try:
			for i in os.listdir(path):
				item.append(i)
			return item
		except:
			return item

	def checkInDir(self,path,file):
		same = False
		for i in os.listdir(path):
			if i == file:
				same = True
		return same

	def getDirSave(self):
		try:
			proPath = self.dirPro_editline.text()
			typ = self.step_combobox.currentText()
			name = self.name_lineEdit.text()
			path = '%s/work/assets/%s/%s/%s/maya/scenes'%(proPath, typ, name)
			return path
		except:
			path = ' '
			return path

	def getFileSave(self,path):
		self.fileSave_lisWidget.clear() 
        #path = r'E:\SILPAKORN\intern\Intern_Project\HouseInForest\SIF\work\assets\char\hashiv\Model\maya\scenes'
		item = self.lisInDir(path)
		iconPath = r"%s/icon/maIcon.png"%PATH
		self.pic_icon = QIcon(iconPath)
		for i in range(len(item)):
			itemList = 'self.pic%02d_widgetItem'%i
			itemList = QListWidgetItem()
			itemList.setIcon(self.pic_icon)
			itemList.setText(item[i])
			self.fileSave_lisWidget.addItem(itemList)

	def getListShot(self,path):
		self.shotSave_lisWidget.clear() 
		item = self.lisInDir(path)
		iconPath = r"%s/icon/maIcon.png"%PATH
		self.pic_icon = QIcon(iconPath)
		for i in range(len(item)):
			itemList = 'self.pic%02d_widgetItem'%i
			itemList = QListWidgetItem()
			itemList.setIcon(self.pic_icon)
			itemList.setText(item[i])
			self.shotSave_lisWidget.addItem(itemList)


	def getSuq(self,path):
		self.seq_combobox.clear()
		item = self.lisInDir(path)
		for i in range(len(item)):
			self.seq_combobox.addItem(item[i])

	def getShot(self):
		proPath = self.dirPro_editline.text()
		seq = self.seq_combobox.currentText()
		path = '%s/work/sequences/%s'%(proPath,seq)
		item = self.lisInDir(path)
		self.shot_combobox.clear()
		for i in range(len(item)):
			self.shot_combobox.addItem(item[i])

	def getName(self):
		proPath = self.dirPro_editline.text()
		typ = self.type_combobox.currentText()
		path = '%s/work/assets/%s'%(proPath,typ)
		item = self.lisInDir(path)
		self.name_combobox.clear()
		for i in range(len(item)):
			self.name_combobox.addItem(item[i])

	def dirBrows(self):
		fileDir = QFileDialog.getExistingDirectory()
		self.dirPro_editline.setText(fileDir)
		proPath = self.dirPro_editline.text()
		path = '%s/work/sequences'%proPath
		self.seq_combobox.clear()
		self.shot_combobox.clear()

		self.getSuq(path)
		self.getName()
		self.find('asset')
		self.find('shot')
#--------------- Function Buttion --------------#
	def saveFile(self):
		proPath = self.dirPro_editline.text()
		typ = self.type_combobox.currentText()
		name = self.name_combobox.currentText()
		step = self.step_combobox.currentText()
		task = self.task_combobox.currentText()

		namePro = proPath.split('/')[-1]
		rootPath = proPath.split('/%s'%namePro)[0]
		fileGen = gf.GenFolAsset(rootPath,namePro)
		fileGen.genName(name, typ)

		softwart = ''
		if step == 'model':
			softwart = 'maya'
		elif step == 'texture':
			if task == 'lo':
				softwart = 'photoshop'
			elif task == 'hi':
				softwart = 'substance'
		elif step == 'lookdev':
			softwart = 'maya'
		elif step == 'rig':
			softwart = 'maya'
		elif step == 'dressing':
			softwart = 'maya'

		path = '%s/work/assets/%s/%s/%s/%s/scenes'%(proPath, typ, name, step.capitalize(),softwart)
		obj = sf.FileUtil(path, typ, name, step, task)
		obj.saveFile()

		self.getFileSave(path)

	def saveShot(self):
		seq = self.seq_combobox.currentText()
		shot = self.shot_combobox.currentText()
		step = self.stepSeq_combobox.currentText()
		task = self.taskSeq_combobox.currentText()
		proPath = self.dirPro_editline.text()
		software = 'maya'
		if step == 'composite':
			software = 'nuke'
		#print seq, shot, step, task, proPath

		path = '%s/work/sequences/%s/%s/%s/%s/scenes'%(proPath, seq, shot, step.capitalize(), software)
		obj = sf.FileUtil(path, seq, shot, step, task)
		obj.saveFile()

		self.getListShot(path)

	def find(self, lis):
		nameList = lis
		proPath = self.dirPro_editline.text()
		if nameList == 'shot':
			seq = self.seq_combobox.currentText()
			shot = self.shot_combobox.currentText()
			step = self.stepSeq_combobox.currentText()
			task = self.taskSeq_combobox.currentText()
			
			software = 'maya'
			if step == 'composite':
				software = 'nuke'

			path = '%s/work/sequences/%s/%s/%s/%s/scenes'%(proPath, seq, shot, step.capitalize(), software)
			self.getListShot(path)
		elif nameList == 'asset':
			typ = self.type_combobox.currentText()
			name = self.name_combobox.currentText()
			step = self.step_combobox.currentText()
			task = self.task_combobox.currentText()
			softwart = ''
			if step == 'model' or step == 'lookdev' or step == 'rig' or step == 'dressing':
				softwart = 'maya'
			elif step == 'texture':
				if task == 'lo':
					softwart = 'photoshop'
				elif task == 'hi':
					softwart = 'substance'

			path = '%s/work/assets/%s/%s/%s/%s/scenes'%(proPath, typ, name, step.capitalize(),softwart)
			self.getFileSave(path)

	def open(self, lis):
		nameList = lis
		proPath = self.dirPro_editline.text()
		if nameList == 'shot':
			seq = self.seq_combobox.currentText()
			shot = self.shot_combobox.currentText()
			step = self.stepSeq_combobox.currentText()
			task = self.taskSeq_combobox.currentText()
			nameFile = self.shotSave_lisWidget.currentItem().text()
			software = 'maya'
			if step == 'composite':
				software = 'nuke'
			path = '%s/work/sequences/%s/%s/%s/%s/scenes'%(proPath, seq, shot, step.capitalize(), software)
			path = '%s/%s'%(path,nameFile)
			mc.file(new = True, force = True )
			mc.file(path, o=True)
			#print path
			
		elif nameList == 'asset':
			typ = self.type_combobox.currentText()
			name = self.name_combobox.currentText()
			step = self.step_combobox.currentText()
			task = self.task_combobox.currentText()
			nameFile = self.fileSave_lisWidget.currentItem().text()
			softwart = ''
			if step == 'model' or step == 'lookdev' or step == 'rig' or step == 'dressing':
				softwart = 'maya'
			elif step == 'texture':
				if task == 'lo':
					softwart = 'photoshop'
				elif task == 'hi':
					softwart = 'substance'
			path = '%s/work/assets/%s/%s/%s/%s/scenes'%(proPath, typ, name, step.capitalize(),softwart)
			path = '%s/%s'%(path,nameFile)
			mc.file(new = True, force = True )
			mc.file(path, o=True)		

	def export(self,):
		abc = self.abc_checkBox.isChecked()
		gpu = self.gpu_checkBox.isChecked()
		ma = self.ma_checkBox.isChecked()
		obj = self.obj_checkBox.isChecked()
		sel = mc.ls(sl = True)
		if abc:
			stripNamespaces = self.stripNamespaces_checkbox.isChecked()
			uvWrite = self.uvWrite_checkbox.isChecked()
			worldSpace = self.worldSpace_checkbox.isChecked()
			writeVisibility = self.writeVisibility_checkbox.isChecked()
			start = int(self.start_lineEdit.text())
			end = int(self.end_lineEdit.text())
			obj1 = eu.ExportAbc(sel, stripNamespaces, uvWrite, worldSpace, 
								writeVisibility, start, end)
			obj1.exportFile()
		if gpu:
			writeMaterials = self.writeMaterials_checkbox.isChecked()
			start = int(self.start_lineEdit.text())
			end = int(self.end_lineEdit.text())
			obj2 = eu.ExportGpu(sel, writeMaterials, start, end)
			obj2.exportFile()
		if ma:
			obj3 = eu.exportMa(sel)
			obj3.exportFile()

		if obj:
			obj4 = eu.ExportObj(sel)
			obj4.exportFile()

	def split(self):
		obj = ss.SplitShot()
		obj.split_shot()

	def import_standin(self):
		proPath = self.dirPro_editline.text()
		typ = self.stnImportType_combobox.currentText()
		name = self.stnImportName_combobox.currentText()
		proPath += '/publish/assets/%s/%s/Cache/'%(typ,name)
		fileName = ''
		lisFile = self.lisInDir(proPath)
		for file in lisFile:
			if '.ass' in file:
				fileName = file
		proPath += fileName

		obj = ms.ManageStandIn()
		obj.import_standin(proPath,name)

	def export_standin(self):
		obj = ms.ManageStandIn()
		obj.export_standin()




def main():
	if not sys.argv[0].endswith('maya.exe'):
		app = QApplication(sys.argv)
		myDialog = saveFileGUI()
		myDialog.show()
		sys.exit(app.exec_())

	else:
		from shiboken2 import wrapInstance
		import maya.OpenMayaUI as omui

		global maya_myDialog
		ptr = wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)
		try:
			maya_myDialog.close()
		except:
			pass
		maya_myDialog = saveFileGUI(parent=ptr)
		maya_myDialog.show()


if __name__ == '__main__':
	main()