import maya.cmds as mc
import pprint
import re

ANIMFRAME_START = 1001

class SplitShot(object):
	"""docstring for SplitShot"""
	def __init__(self):
		super(SplitShot, self).__init__()
		self.shot = []
		self.animcurve = []
		self.fileP = mc.file(q=True,exn=True) # path+name.ma
		self.fileName = re.findall(r'\w+',self.fileP)[-2] #name #not .ma
		self.filePath = re.split(r'\w+_\w+_\w+_\w+_v[0-9]{3}.ma',self.fileP)[0] # path of file

	def lis_shot(self):
		self.shot = mc.ls(type = 'shot')
		return self.shot

	def lis_animcurve(self):
		self.animcurve = mc.ls(type = 'animCurve')
		return self.animcurve

	def edit_filename(self,shot):
		oldname = re.findall(r'\w+_(\w+)_\w+_\w+_\w+',self.fileName)[0]
		if len(re.findall(r'\w+_(\w+)_\w+',shot)) > 0:
			name = re.findall(r'\w+_(\w+)_\w+',shot)[0]
			name = self.fileName.replace(oldname,name)
			stepOld = re.findall(r'\w+_\w+_(\w+)_\w+_\w+',self.fileName)[0]
			name = name.replace(stepOld,'animation')
			versionOld = re.findall(r'(_v[0-9]{3})',name)[0]
			name = name.replace(versionOld,'_v001')
			return name

	def edit_path(self,shot):
		oldname = re.findall(r'\w+_(\w+)_\w+_\w+_\w+',self.fileName)[0]
		if len(re.findall(r'\w+_(\w+)_\w+',shot)) > 0:
			name = re.findall(r'\w+_(\w+)_\w+',shot)[0]
			path = self.filePath.replace(oldname,name)
			stepOld = re.findall(r'\w+_\w+_(\w+)_\w+_\w+',self.fileName)[0]
			path = path.replace(stepOld.capitalize(),'Animation')
			return path

	def split_shot(self):
		shot_lis = self.lis_shot()
		animcurve_lis = self.lis_animcurve()
		for shot in shot_lis:
			name = self.edit_filename(shot)
			path = self.edit_path(shot)
			print '%s%s'%(path,name)
			mc.file(rename='%s%s.ma'%(path,name))
			mc.file(save=True, type="mayaAscii")
			start_frame = mc.shot(shot,q = True, st = True)
			end_frame = mc.shot(shot,q = True, et = True)
			#print start_frame,end_frame
			sub = start_frame - ANIMFRAME_START
			if start_frame > ANIMFRAME_START:
				for animcurve in animcurve_lis:
					mc.keyframe(animcurve, e = True, option='over',
											r=True, tc=-(sub),iub=True)
			
			end_frame = end_frame-sub
			mc.playbackOptions(aet = end_frame)
			mc.playbackOptions(max = end_frame)
			mc.file(save=True,type="mayaAscii")

			if start_frame > ANIMFRAME_START: 
				for animcurve in animcurve_lis:
					mc.keyframe(animcurve, e = True, option='over',
											r=True, tc=+(sub),iub=True)

			
			#keyframe -animation keys -option over -relative -timeChange (0 - 50) ;

		
