import maya.cmds as mc
import re
import json
import os
import manageStandIn as ms
from pprint import pprint


class Position(object):
	def __init__(self):
		super(Position, self).__init__()
		self.path = self.fileP = mc.file(q=True,exn=True)
		self.nameFile = re.findall(r'\w+',self.fileP)[-2]
		self.path = self.path.split(self.nameFile)[0]

	def rePath(self):
		self.path = self.path.replace('work','publish')
		step = re.findall(r'\w+_\w+_(\w+)_\w+_v[0-9]{3}',self.nameFile)[0]
		self.path = self.path.split(step.capitalize())[0]
		return self.path

	def renameJSON(self):
		step = re.findall(r'\w+_\w+_(\w+)_\w+_v[0-9]{3}',self.nameFile)[0]
		self.nameFile = self.nameFile.replace(step,'position')
		task = re.findall(r'\w+_\w+_\w+_(\w+)_v[0-9]{3}',self.nameFile)[0]
		self.nameFile = self.nameFile.replace(task,'json')
		version = re.findall(r'\w+_v([0-9]{3})',self.nameFile)[0]
		self.nameFile = self.nameFile.replace(version,'001')
		return self.nameFile

	def lisInDirAss(self,path):
		item = []
		assFile = []
		try:
			for i in os.listdir(path):
				item.append(i)
		except:
			pass
		for i in item:
			if '.ass' in i:
				assFile.append(i)
		return assFile


	def export_position(self):
		sels = mc.ls(sl=True)
		treePos_dict = {}
		treegrp_dict = {}	
		origObject = {}
		instancedObject = []
		position_tree_dict = {}
		for sel in sels:
			children = mc.listRelatives(sel, ad=True, type='transform')
			print sel
			path = 'E:/SILPAKORN/intern/Intern_Project/HouseInForest/SIF/publish/assets/env/%s/Cache'%sel
#			print path
			nameFile = self.lisInDirAss(path)[0]
 			instancedObject = []
			for chid in children:
				treegrp_dict = {}	
				if '00' in chid:
					origObject = {}
					origObject['namespace'] = chid
					origObject['path'] = '%s/%s'%(path,nameFile)
					origObject['transformMatrix'] = mc.xform(chid,q=True, m=True)
				else:
					instance_dict = {}
					instance_dict['namespace'] = chid
					instance_dict['transformMatrix'] = mc.xform(chid,q=True, m=True)
					instancedObject.append(instance_dict)
				treegrp_dict['origObject'] = origObject
				treegrp_dict['instancedObject'] = instancedObject
				treePos_dict[sel.split('|')[-1]] = treegrp_dict
#		pprint(treePos_dict)
 		jsonPath = '%s%s.json'%(self.rePath(),self.renameJSON())
#		print jsonPath
		with open(jsonPath, 'w') as outfile: 
			json.dump(treePos_dict, outfile, indent=4)
		return jsonPath

	def import_position(self,path):#path = path+name of json
		position_dict = {}
		with open(path, 'r') as json_data:
			position_dict = json.load(json_data)
		return position_dict

	def replacePosition(self,path):#path = path+name of json
		forest = self.import_position(path)
		origPath = ''
		for tree in forest:
			origData = forest[tree]['origObject']
			origNamespace = origData['namespace']
			origMatrix = origData['transformMatrix']
			origPath = origData['path']
			obj = ms.ManageStandIn()
			obj.import_standin(path = origPath, name = origNamespace.replace('PLY','STN'))
		
		for tree in forest:
			instTrees = forest[tree]['instancedObject']
			origData = forest[tree]['origObject']
			origNamespace = origData['namespace'].replace('PLY','STN')
			for instTree in instTrees:
				instNamespace = instTree['namespace']
				instMatrix = instTree['transformMatrix']
				instName = mc.instance(origNamespace,n = instNamespace.replace('PLY','STN'))[0]
				mc.xform(instName,m=instMatrix)


